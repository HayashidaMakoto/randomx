# RandomX

RandomX is a proof of work algorithm based on random code execution.
The reference implementation is available on
[GitHub](https://github.com/tevador/RandomX).

This implementation in Rust aims to follow the specification described in the
original implementation.
